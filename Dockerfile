FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="maintainer@gmail.com"
# we could have used the famous version of nginx but with this image the default user is 
# the root and it's not recommanded that's why it's best practise touse image with a least-priviledged user 


COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
# we store the nginx conf here: /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params
# we store ./uwsgi_params here : /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000
# we set some ENV's variable

USER root
# we need to run some command as root user in order to create directories and files

RUN mkdir -p /vol/static
# we create /vol/static directories -p means taht we create a vol and a static in vol directories
RUN chmod 755 /vol/static
# https://chmod-calculator.com/
# chmod set permissions for the directory /vol/static 
# it means that owner can read, execute and write in this directory
# Groud and Public can only read and execute it 
# cela nous permet de serve static files grace a notre proxy nginx 
RUN touch /etc/nginx/conf.d/default.conf
# on cree ce fichier comme user : root car qd on switch to nginx user , 
# nginx pourra modifier le fichier de conf de nginx mais ne peut pas le creer car pas la permission 
# donc on doit le creer avant et avec nginx user on pourra changer les variables avec les vraiesvaleurs des variables 
# VOIR Readme
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
# permet a user nginx et group nginx => nginx:nginx  de modifier le fichier (on lui donne la permission )
COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh
# rend un fichier executable , on veut que ce fichier soit executable pour qu'il soit executer qd docker run 
USER nginx 
# we switch back with the nginx user

# we run /ntrypoint.sh qui reprensente le fichier a executer qd le docker container s'execute 
CMD ["/entrypoint.sh"]

# ds la command pour le porjet on construit l'image
# docker build -t proxy . => on construit l'image se trouvant ds la current directory ( . ) et avec -t on donne le nom proxy
