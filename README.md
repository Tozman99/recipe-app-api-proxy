# recipe-app-api-proxy

NGINX proxy app for our recipe app API 

## Usage 

### Environnment Variables 

* "LISTEN_PORT" - Port to listen on [default: "8000"]
* "APP_HOST" - Hostname of the app to forward requests to [defautl: "app"]
* "APP_PORT" - Port of the app to forward requests to [default: "9000"]

## Notes 

Aws ECR  = store images like docker hub and based on the iam user policies we can access thoses images or not 


# for creating a new branch : git checkout -b feature/nginx-proxy

we want that because we want to have a protected branch 

# the default.conf.tpl is used to set the configuration of nginx server 
# .tpl is used for populating value based on environnement variables 
# location /static {
        alias /vol/static;
    }
    is used to tell nginx server to handle static files cause nginx is more efficient than django for serving static files 
    and all the request for static files are performed by nginx 
    /static all the request for static files are /static 

    location / {
        uwsgi_pass              #{APP_HOST}:${APP_PORT};
        include                 /etc/nginx/uwsgi_params;
        client_max_body_size    10M;

    }
    this location means that for the rest of the request we use uwsgi to handle them by adding new params
    #https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html#what-is-the-uwsgi-params-file

    client_max_body_size  10M means that a req.body has a max size of 10 megabites normally it's less than it but sometimes a img can be between 1M and 10 M 


# entrypoint.sh => le fichier qui va etre exectuter qd le docker container run 

#!/bin/sh => permet dire a l'unix image que c'est le fichier a executer (entrypoint.sh)

set -e => permet d'afficher les erreurs qd le docker container run pour debug + facilement 

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf 

=> jusqu'a mtn les variables sont de ce format : ${APP_PORT} et pour leur donner leurs vraies valeurs : il faut utiliser la commande envsubst pour obtenir les variables /etc/nginx/conf.d/default.conf est l'endpoint pour lequel nginx s'attend a avoir sa configuration 
nginx -g 'daemon off;' dit que nginx s'execute en non pas en background mais en foreground car 
cela permet d'obtenir les outputs de nginx comme input de docker 


# .gitlab-ci.yml
# ce fichier est executer par gitlab pour CI/ CD continous integration


https://docs.gitlab.com/ee/topics/gitlab_flow.html => explique le flow de gitlab 
https://docs.gitlab.com/ee/ci/variables/predefined_variables.html => explique stages and jobs pareil pour celui en dessous 
https://docs.gitlab.com/ee/ci/yaml/#rules

BINE DETERMINER BRANCHING STRATEGIES 
meme si mtn on utilise + une continius integration 
d'abord on determine l'image image: 

docker:19.03.5
services:
  - docker:19.03.5-dind

comme pour docker-compose.yml 
on determine les services qu''on va utiliser pour notre app 
- docker:19.03.5-dind
cela permet d'utiliser docker dans docker 


stages:
  - Build
  - Push

on a plrs stages d'abord le stage build 
qui permet de creer notre proxy image

et push stage qui va permet de push l'image vers ECR 

Build: => c'est un job qui va ns permettre de run plrs command 

  stage: Build => permet d'affecter un job a un stage 
  on peut avoir plrs jobs pour un stage
  before_script: []
  script: => les command qui vont etre executer pour ce job 
    - mkdir data/ : on cree data/
    - docker build --compress -t proxy . # on cree un image le nom est proxy et --compress = on diminue la size de l'image (il faut utiliser --compress en production )
    - docker save --output data/image.tar proxy
    # on savaugarde notre image ds data/image.tar sous le nom de proxy cela nous permet d'utiliser artifacts 
  artifacts:
    name: image
    paths:
      - data/

artifacts est un moyen d'entre des fichiers d'un job vers le prochain job 
ici pour le prochain job on aura data/ qui sera "disponible" pour notre prochain job qui sera push pour push notre project (build an image )


si on definit pas un artifact alors puisque chaque job refresh les files on aura plus notre images 


before_script:
  - apk add python3 => on install python3 
  - pip3 install awscli==1.18.8 => on installe awscli 
  - docker load --input data/image.tar => commande permettant de charger l'image ds artifacts pour pouvoir l'utiliser ds push 
  - $(aws ecr get-login --no-include-email --region us-east-1) => donne la possibilite de se connecter avec docker 

before_script is script that are executed for each jobs it prevents for repeatitive command so if jobs has same command scripts then it's best practise to use before_Script 

Si un job ne doit pas avoir les scripts alors on override before_scripts: []
comme ca la valeurs de before_Scripts pour ce job = []


 - docker tag proxy:latest $ECR_REPO:Dev => when we build our image it means that it corespond to the latest tag of the proxy image 
 - docker push $ECR_REPO:Dev => we push the image to the repo with dev
  rules: 
    - if: "$CI_COMMIT_BRANCH == 'master'" => ce job est execute ssi 
    nous sommes ds la branche master 


  - export TAGGED_ECR_REPO=$ECR_REPO:$(echo $CI_COMMIT_TAG | sed 's/-release//')

$CI_commit_TAG = une var qui correspond au tag lorsqu'un job est execute 
sed ... veut dire qu'on va retirer la parti -release du tag et de ce fait on aura un tag du style : 1.0.0 au lieu de 1.0.0-release ensuite on store le tag EX: 1.0.0 ds TAGGED_ECR_REPO


  - docker tag proxy:latest $TAGGED_ECR_REPO 
=> avant on a creer notre tag et stocke ds #TAGGED_ECR_REPO, on definit ce tag comme etant le latest

  - docker push $TAGGED_ECR_REPO
=> on push le tag 

  - docker tag $TAGGED_ECR_REPO $ECR_REPO:latest
    on definit que notre tag est le latest tag

  - docker push $ECR_REPO:latest
  on push le tag latest

    rules:
  - if: "$CI_COMMIT_TAG =~ /*-release$/"

  Si on a un tag qui possede a la fin -release alors on applique ce job 